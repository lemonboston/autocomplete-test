package com.gk.test1.data.server;

import org.dmfs.httpclientinterfaces.HttpMethod;
import org.dmfs.httpclientinterfaces.HttpRequest;
import org.dmfs.httpclientinterfaces.HttpRequestEntity;
import org.dmfs.httpclientinterfaces.HttpResponse;
import org.dmfs.httpclientinterfaces.HttpResponseHandler;
import org.dmfs.httpclientinterfaces.exceptions.ProtocolError;
import org.dmfs.httpclientinterfaces.exceptions.ProtocolException;
import org.dmfs.httpclientinterfaces.headers.HeaderList;
import org.dmfs.httpclientinterfaces.headers.impl.EmptyHeaderList;

import java.io.IOException;

/**
 * A 'fake' {@link HttpRequest} for the registration suggestion request.
 *
 * @author Gabor_Keszthelyi
 */
public class RegistrationSuggestionsRequest implements HttpRequest<RegistrationSuggestionResponse> {

    public final String query;

    public RegistrationSuggestionsRequest(String query) {
        this.query = query;
    }

    @Override
    public HttpMethod method() {
        return HttpMethod.GET;
    }

    @Override
    public HeaderList headers() {
        return EmptyHeaderList.INSTANCE;
    }

    @Override
    public HttpRequestEntity requestEntity() {
        return null;
    }

    @Override
    public HttpResponseHandler<RegistrationSuggestionResponse> responseHandler(HttpResponse response) throws IOException, ProtocolError, ProtocolException {
        return null;
    }

}
