package com.gk.test1.data.client;

import java.util.List;

/**
 * Data model for the suggestions for the user for registration (email, serviceJsons) based on their username fraction entered.
 *
 * @author Gabor_Keszthelyi
 */
public class RegistrationSuggestions {

    private String originalText;
    private boolean isExactMatch;
    private List<String> emailSuggestions;

    // TODO Exact matches data would come here too (if I knew more what parts of them and how would be used)

    public boolean isExactMatch() {
        return isExactMatch;
    }

    public String getSingleMatchDomain() {
        return "GMX"; // TODO
    }

    public boolean hasEmailSuggestions() {
        return emailSuggestions != null && !emailSuggestions.isEmpty();
    }

    public List<String> getEmailSuggestions() {
        return emailSuggestions;
    }

    public void setEmailSuggestions(List<String> emailSuggestions) {
        this.emailSuggestions = emailSuggestions;
    }

    public String getOriginalText() {
        return originalText;
    }

    public void setOriginalText(String originalText) {
        this.originalText = originalText;
    }

    public void setExactMatch(boolean exactMatch) {
        this.isExactMatch = exactMatch;
    }

}
