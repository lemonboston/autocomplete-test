package com.gk.test1.data.server;

import java.util.List;

/**
 * Data class for the parsed json response element 'exact_matches'.
 *
 * @author Gabor_Keszthelyi
 */
public class ExactMatchesJson {

    public List<String> domains;
    public List<ServiceJson> services;
    public String id;
    public String name;

}
