package com.gk.test1.di;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Scope for objects that need the same lifetime as the Activity.
 *
 * @author Gabor_Keszthelyi
 */
@Scope
@Retention(RUNTIME)
public @interface ActivityScope {
}
