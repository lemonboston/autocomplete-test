package com.gk.test1.network.retrofit;

import com.gk.test1.data.server.RegistrationSuggestionResponse;
import com.gk.test1.data.server.RegistrationSuggestionsRequest;

import org.dmfs.httpclientinterfaces.HttpRequest;
import org.dmfs.httpclientinterfaces.HttpRequestExecutor;
import org.dmfs.httpclientinterfaces.OnRedirectCallback;
import org.dmfs.httpclientinterfaces.OnResponseCallback;
import org.dmfs.httpclientinterfaces.exceptions.ProtocolError;
import org.dmfs.httpclientinterfaces.exceptions.ProtocolException;
import org.dmfs.httpclientinterfaces.exceptions.RedirectionException;
import org.dmfs.httpclientinterfaces.exceptions.UnexpectedResponseException;

import java.io.IOException;
import java.net.URI;

/**
 * 'Fake' {@link HttpRequestExecutor}, handling only {@link RegistrationSuggestionsRequest} with Retrofit http client.
 *
 * @author Gabor_Keszthelyi
 */
public class FakeHttpRequestExecutor implements HttpRequestExecutor {

    private final RegistrationSuggestionApi registrationSuggestionApi;

    public FakeHttpRequestExecutor(RegistrationSuggestionApi registrationSuggestionApi) {
        this.registrationSuggestionApi = registrationSuggestionApi;
    }

    @Override
    public <T> T execute(URI uri, HttpRequest<T> request) throws IOException, ProtocolError, ProtocolException, RedirectionException, UnexpectedResponseException {
        if (request instanceof RegistrationSuggestionsRequest) {
            String query = ((RegistrationSuggestionsRequest) request).query;
            RegistrationSuggestionResponse response = registrationSuggestionApi.queryForSuggestions(query).execute().body();
            return (T) response;
        }
        return null;
    }

    @Override
    public <T> T execute(URI uri, HttpRequest<T> request, OnRedirectCallback redirectionCallback) throws IOException, ProtocolError, ProtocolException, RedirectionException, UnexpectedResponseException {
        return null;
    }

    @Override
    public <T> void execute(URI uri, HttpRequest<T> request, final OnResponseCallback<T> callback) {

    }

    @Override
    public <T> void execute(URI uri, HttpRequest<T> request, OnResponseCallback<T> callback, OnRedirectCallback redirectionCallback) {

    }
}
