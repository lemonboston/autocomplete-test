package com.gk.test1.base;

import android.support.v7.app.AppCompatActivity;

/**
 * Base class for all activities in the app.
 *
 * @author Gabor_Keszthelyi
 */
public class BaseActivity extends AppCompatActivity {
}
