package com.gk.test1.framework.access;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;

import com.gk.test1.base.BaseActivity;


/**
 * Default implementation of {@link ViewToolkit}.
 *
 * @author Gabor_Keszthelyi
 */
public class ViewToolkitImpl implements ViewToolkit {

    private final BaseActivity activity;

    public ViewToolkitImpl(BaseActivity activity) {
        this.activity = activity;
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        activity.setContentView(layoutResID);
    }

    @Override
    public <T extends ViewDataBinding> T setContentViewBinding(@LayoutRes int layoutResID) {
        return DataBindingUtil.setContentView(activity, layoutResID);
    }

}
