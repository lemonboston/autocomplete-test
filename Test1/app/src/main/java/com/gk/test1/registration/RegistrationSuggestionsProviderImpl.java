package com.gk.test1.registration;

import android.util.Log;

import com.gk.test1.data.client.RegistrationSuggestions;
import com.gk.test1.data.convert.RegistrationSuggestionsConverter;
import com.gk.test1.data.server.RegistrationSuggestionResponse;
import com.gk.test1.data.server.RegistrationSuggestionsRequest;
import com.gk.test1.network.NetworkException;

import org.dmfs.httpclientinterfaces.HttpRequestExecutor;

import java.util.concurrent.TimeUnit;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

/**
 * Implementation for {@link RegistrationSuggestionsProvider}.
 *
 * @author Gabor_Keszthelyi
 */
public class RegistrationSuggestionsProviderImpl implements RegistrationSuggestionsProvider {

    private static final String TAG = RegistrationSuggestionsProviderImpl.class.getSimpleName();

    private final PublishSubject<String> pipeline;
    private final HttpRequestExecutor httpExecutor;
    private final EmailUtils emailUtils;
    private final RegistrationSuggestionsConverter converter;

    private Listener listener;

    public RegistrationSuggestionsProviderImpl(final HttpRequestExecutor httpExecutor, EmailUtils emailUtils, RegistrationSuggestionsConverter converter) {
        this.emailUtils = emailUtils;
        this.converter = converter;
        this.pipeline = PublishSubject.create();
        this.httpExecutor = httpExecutor;

        setupPipeline();

    }

    private void setupPipeline() {
        pipeline
                .subscribeOn(Schedulers.io())
                .debounce(WAIT_WHILE_TYPING_DELAY, TimeUnit.MILLISECONDS)
                .filter(new Func1<String, Boolean>() {
                    @Override
                    public Boolean call(String textTyped) {
                        return emailUtils.containsDomainFraction(textTyped);
                    }
                })
                .observeOn(Schedulers.newThread())  // TODO Changing threads here to fix InterruptedIOException thrown by okio. Delete when real HttpRequestExecutor is used.
                .map(new Func1<String, RegistrationSuggestions>() {
                    @Override
                    public RegistrationSuggestions call(String emailFraction) {
                        return fetchSuggestions(emailFraction);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<RegistrationSuggestions>() {
                    @Override
                    public void call(RegistrationSuggestions suggestions) {
                        if (listener != null) {
                            listener.onSuggestionsReceived(suggestions);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.e(TAG, "Error during email text suggestion processing", throwable);
                    }
                });
    }

    private RegistrationSuggestions fetchSuggestions(String emailFraction) {
        Log.d(TAG, "Processing text:" + emailFraction);
        String domain = emailUtils.getDomainFraction(emailFraction);
        RegistrationSuggestionResponse response = executeRequest(domain);
        RegistrationSuggestions suggestions = converter.convert(response, emailFraction);
        return suggestions;
    }

    private RegistrationSuggestionResponse executeRequest(String domain) {
        try {
            return httpExecutor.execute(null, new RegistrationSuggestionsRequest(domain));
        } catch (Exception e) {
            throw new NetworkException(e);
        }
    }

    @Override
    public void registerListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public void unregisterListener() {
        this.listener = null;
    }

    @Override
    public void onNewUserNameText(String newText) {
        pipeline.onNext(newText);
    }
}
