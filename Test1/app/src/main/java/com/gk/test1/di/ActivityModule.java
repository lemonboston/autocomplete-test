package com.gk.test1.di;


import com.gk.test1.base.BaseActivity;
import com.gk.test1.data.convert.RegistrationSuggestionsConverter;
import com.gk.test1.framework.access.StringResAccess;
import com.gk.test1.framework.access.ViewToolkit;
import com.gk.test1.framework.access.ViewToolkitImpl;
import com.gk.test1.registration.EmailUtils;
import com.gk.test1.registration.RegistrationController;
import com.gk.test1.registration.RegistrationSuggestionsProvider;
import com.gk.test1.registration.RegistrationSuggestionsProviderImpl;
import com.gk.test1.registration.RegistrationView;
import com.gk.test1.registration.RegistrationViewImpl;

import org.dmfs.httpclientinterfaces.HttpRequestExecutor;

import dagger.Module;
import dagger.Provides;

/**
 * Dagger Module for Activities.
 *
 * @author Gabor_Keszthelyi
 */
@Module
public class ActivityModule {

    private final BaseActivity activity;

    public ActivityModule(BaseActivity activity) {
        this.activity = activity;
    }

    @ActivityScope
    @Provides
    public ViewToolkit provideViewToolkit() {
        return new ViewToolkitImpl(activity);
    }

    @ActivityScope
    @Provides
    public RegistrationController provideRegistrationController(RegistrationView view, RegistrationSuggestionsProvider registrationSuggestionsProvider, StringResAccess stringResAccess) {
        return new RegistrationController(view, registrationSuggestionsProvider, stringResAccess);
    }

    @ActivityScope
    @Provides
    public RegistrationView provideRegistrationView(ViewToolkit viewToolkit) {
        return new RegistrationViewImpl(viewToolkit);
    }

    @ActivityScope
    @Provides
    public RegistrationSuggestionsProvider usernameSuggestionProvider(HttpRequestExecutor httpExecutor, EmailUtils emailUtils, RegistrationSuggestionsConverter converter) {
        return new RegistrationSuggestionsProviderImpl(httpExecutor, emailUtils, converter);
    }

    @Provides
    public EmailUtils emailUtils() {
        return new EmailUtils();
    }

    @Provides
    public RegistrationSuggestionsConverter provideRegistrationSuggestionsConverter(EmailUtils emailUtils) {
        return new RegistrationSuggestionsConverter(emailUtils);
    }

}
