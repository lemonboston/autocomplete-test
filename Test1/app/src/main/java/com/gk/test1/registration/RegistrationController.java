package com.gk.test1.registration;

import android.util.Log;

import com.gk.test1.R;
import com.gk.test1.data.client.RegistrationSuggestions;
import com.gk.test1.framework.access.StringResAccess;
import com.gk.test1.framework.dispatch.OnCreate;
import com.gk.test1.framework.dispatch.OnDestroy;

/**
 * Controller class for the Registration screen.
 * <p>
 * (Note: Controller is a standard part of this presentation layer architectural approach, they are similar to MPV / VIPER Presenters, just this name fits the responsibilities better imo.)
 *
 * @author Gabor_Keszthelyi
 */
public class RegistrationController implements OnCreate, OnDestroy, RegistrationView.UserActionListener, RegistrationSuggestionsProvider.Listener {

    private static final String TAG = RegistrationController.class.getSimpleName();

    private static final int MIN_USER_NAME_LENGTH = 3;

    private final RegistrationView view;
    private final RegistrationSuggestionsProvider suggestionsProvider;
    private final StringResAccess stringResAccess;
    private final String defaultSignInButtonText;

    private RegistrationSuggestions suggestion;

    public RegistrationController(RegistrationView view, RegistrationSuggestionsProvider suggestionsProvider, StringResAccess stringResAccess) {
        this.view = view;
        this.suggestionsProvider = suggestionsProvider;
        this.stringResAccess = stringResAccess;
        this.defaultSignInButtonText = stringResAccess.getString(R.string.registration_sign_in_button);
    }

    @Override
    public void onCreate() {
        view.init();
        view.setUserActionListener(this);
        suggestionsProvider.registerListener(this);
    }

    @Override
    public void onDestroy() {
        suggestionsProvider.unregisterListener();
    }

    @Override
    public void onUsernameCharTyped(String newText, boolean wasCharDeleted) {
        Log.d(TAG, "Text entered: " + newText);
        view.setSignInButtonEnabled(newText.length() >= MIN_USER_NAME_LENGTH);
        view.hideOtherServicesButton();
        view.setSignInButtonText(defaultSignInButtonText);
        if (wasCharDeleted) {
            view.clearSuggestions();
        }
        suggestionsProvider.onNewUserNameText(newText);
    }

    @Override
    public void onSignInButtonClick() {

    }

    @Override
    public void onOtherServicesButtonClick() {

    }

    @Override
    public void onSuggestionsReceived(RegistrationSuggestions suggestions) {
        if (isStillTheSameText(suggestions)) {
            if (suggestions.isExactMatch()) {
                this.suggestion = suggestions;
                view.setSignInButtonText(stringResAccess.getString(R.string.exactSignIn, suggestions.getSingleMatchDomain()));
                view.showOtherServicesButton();
                view.clearSuggestions();
            } else if (suggestions.hasEmailSuggestions()) {
                view.updateSuggestions(suggestions.getEmailSuggestions());
            }
        }
    }

    private boolean isStillTheSameText(RegistrationSuggestions suggestions) {
        return suggestions.getOriginalText().equals(view.getUsernameEntered());
    }
}
