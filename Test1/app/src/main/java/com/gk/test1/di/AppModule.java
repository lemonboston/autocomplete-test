package com.gk.test1.di;

import com.gk.test1.base.App;
import com.gk.test1.framework.access.StringResAccess;
import com.gk.test1.framework.access.StringResAccessImpl;
import com.gk.test1.network.retrofit.FakeHttpRequestExecutor;
import com.gk.test1.network.retrofit.RegistrationSuggestionApi;

import org.dmfs.httpclientinterfaces.HttpRequestExecutor;

import java.security.cert.CertificateException;

import javax.inject.Singleton;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Dagger Module for the global (singleton) objects and for the ones need application Context.
 *
 * @author Gabor_Keszthelyi
 */
@Module
public class AppModule {

    private final App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    public StringResAccess provideStringResAccess() {
        return new StringResAccessImpl(app);
    }

    @Singleton
    @Provides
    public HttpRequestExecutor provideHttpRequestExecutor(RegistrationSuggestionApi registrationSuggestionApi) {
        return new FakeHttpRequestExecutor(registrationSuggestionApi);
    }

    @Singleton
    @Provides
    public RegistrationSuggestionApi provideRegistrationSuggestionApi(OkHttpClient okHttpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RegistrationSuggestionApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        return retrofit.create(RegistrationSuggestionApi.class);
    }

    @Singleton
    @Provides
    public OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder builder = createTrustingOkHttpClient();

        HttpLoggingInterceptor logger = new HttpLoggingInterceptor();
        logger.setLevel(HttpLoggingInterceptor.Level.BODY); // no idea why the compiler warning here, but it builds fine
        builder.addInterceptor(logger);

        return builder.build();
    }

    private OkHttpClient.Builder createTrustingOkHttpClient() {
        try {
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            return builder;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
