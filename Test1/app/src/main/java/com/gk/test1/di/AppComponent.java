package com.gk.test1.di;

import com.gk.test1.framework.access.StringResAccess;

import org.dmfs.httpclientinterfaces.HttpRequestExecutor;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Application (global) scope dagger Component.
 *
 * @author Gabor_Keszthelyi
 */
@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    HttpRequestExecutor getHttpRequestExecutor();

    StringResAccess getStringResAccess();

}
