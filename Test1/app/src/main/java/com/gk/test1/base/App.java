package com.gk.test1.base;

import android.app.Application;

import com.gk.test1.di.Injector;

/**
 * The {@link Application} class for the app.
 *
 * @author Gabor_Keszthelyi
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Injector.init(this);

    }
}
