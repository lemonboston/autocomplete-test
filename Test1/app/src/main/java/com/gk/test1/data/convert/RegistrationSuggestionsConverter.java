package com.gk.test1.data.convert;

import com.gk.test1.data.client.RegistrationSuggestions;
import com.gk.test1.data.server.RegistrationSuggestionResponse;
import com.gk.test1.registration.EmailUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Converts the response data structure of registration suggestions to our client side data model.
 *
 * @author Gabor_Keszthelyi
 */
// TODO review, unit test after clarifications on the response data meanings, handling
public class RegistrationSuggestionsConverter {

    private final EmailUtils emailUtils;

    public RegistrationSuggestionsConverter(EmailUtils emailUtils) {
        this.emailUtils = emailUtils;
    }

    /**
     * Converts the suggestions response and the original text to our client side data model.
     *
     * @param response     the parsed response
     * @param originalText the original text entered by the user
     * @return the new data model object
     */
    public RegistrationSuggestions convert(RegistrationSuggestionResponse response, String originalText) {
        RegistrationSuggestions result = new RegistrationSuggestions();
        result.setOriginalText(originalText);
        result.setExactMatch(assessExactMatch(response));
        result.setEmailSuggestions(createEmailSuggestions(originalText, response.suggestions));
        return result;
    }

    private List<String> createEmailSuggestions(String originalText, List<String> domainSuggestions) {
        List<String> emailSuggestions = new ArrayList<>(domainSuggestions.size());
        for (String domainSuggestion : domainSuggestions) {
            emailSuggestions.add(emailUtils.replaceDomain(originalText, domainSuggestion));
        }
        return emailSuggestions;
    }

    private boolean assessExactMatch(RegistrationSuggestionResponse response) {
        return response.exactMatches != null && !response.exactMatches.isEmpty(); // TODO confirm
    }
}
