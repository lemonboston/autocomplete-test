package com.gk.test1.registration;

import com.gk.test1.data.client.RegistrationSuggestions;

/**
 * Provides email address and services suggestions for the registration based on the username fraction entered by the user.
 *
 * @author Gabor_Keszthelyi
 */
public interface RegistrationSuggestionsProvider {

    int WAIT_WHILE_TYPING_DELAY = 500;

    /**
     * Processes the text entered by the user. If text is an email fraction with at least one char of domain part, it will fetch suggestions from the API.
     * Should be called each time the user enters a new character, it applies a delay, ie not process while user is typing.
     *
     * @param newText the new whole text typed so far by the user
     */
    void onNewUserNameText(String newText);

    /**
     * Registers the listener which will be notified of the fetched suggestions.
     *
     * @param listener listener
     */
    void registerListener(Listener listener);

    /**
     * Unregisters the listener.
     */
    void unregisterListener();

    /**
     * Listener interface used to notify about the fetched and processed suggestions.
     */
    interface Listener {

        /***
         * Called when new suggestions is available.
         *
         * @param suggestions the suggestions
         */
        void onSuggestionsReceived(RegistrationSuggestions suggestions);
    }
}
