package com.gk.test1.data.server;

/**
 * Data class for the parsed json response element of the 'serviceJsons' array.
 *
 * @author Gabor_Keszthelyi
 */
public class ServiceJson {

    public String group;
    public String name;
    public String uri;
    public String auth;
    public String priority;
    public String id;

}
