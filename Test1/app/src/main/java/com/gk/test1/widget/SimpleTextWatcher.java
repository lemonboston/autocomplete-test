package com.gk.test1.widget;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Empty {@link TextWatcher} for simpler usage when only one method is needed to override.
 *
 * @author Gabor_Keszthelyi
 */
public abstract class SimpleTextWatcher implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
