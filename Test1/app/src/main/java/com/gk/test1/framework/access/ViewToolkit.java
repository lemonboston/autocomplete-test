package com.gk.test1.framework.access;

import android.app.Activity;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;

/**
 * Provides the framework functions that a View component may need.
 *
 * @author Gabor_Keszthelyi
 */
public interface ViewToolkit {

    /**
     * @see Activity#setContentView(int)
     */
    void setContentView(@LayoutRes int layoutResID);

    /**
     * @see Activity#setContentView(int)
     */
    <T extends ViewDataBinding> T setContentViewBinding(@LayoutRes int layoutResID);

}
