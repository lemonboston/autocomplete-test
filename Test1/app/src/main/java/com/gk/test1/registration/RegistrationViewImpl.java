package com.gk.test1.registration;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.gk.test1.R;
import com.gk.test1.databinding.ActivityRegistrationBinding;
import com.gk.test1.framework.access.ViewToolkit;
import com.gk.test1.widget.SimpleTextWatcher;

import java.util.List;

/**
 * Implementation for {@link RegistrationView}.
 *
 * @author Gabor_Keszthelyi
 */
public class RegistrationViewImpl implements RegistrationView {

    private final ViewToolkit viewToolkit;

    private UserActionListener listener;
    private AutoCompleteTextView autoCompleteText;
    private ArrayAdapter<String> adapter;
    private Button signInButton;
    private Button otherServicesButton;

    public RegistrationViewImpl(ViewToolkit viewToolkit) {
        this.viewToolkit = viewToolkit;
    }

    @Override
    public void init() {
        ActivityRegistrationBinding binding = viewToolkit.setContentViewBinding(R.layout.activity_registration);
        autoCompleteText = binding.autoCompleteEditText;
        signInButton = binding.signInButton;
        otherServicesButton = binding.otherServicesButton;

        autoCompleteText.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence newCharSequence, int start, int before, int count) {
                listener.onUsernameCharTyped(newCharSequence.toString(), before != 0);
            }
        });

        adapter = new ArrayAdapter<>(autoCompleteText.getContext(), android.R.layout.simple_dropdown_item_1line, new String[0]);
        autoCompleteText.setAdapter(adapter);
    }

    @Override
    public void setUserActionListener(UserActionListener userActionListener) {
        this.listener = userActionListener;
    }

    @Override
    public void updateSuggestions(List<String> emailSuggestions) {
        adapter.clear();
        adapter.addAll(emailSuggestions);
        adapter.getFilter().filter(autoCompleteText.getText());
    }

    @Override
    public void setSignInButtonText(String text) {
        signInButton.setText(text);
    }

    @Override
    public void setSignInButtonEnabled(boolean enabled) {
        signInButton.setEnabled(enabled);
    }

    @Override
    public void showOtherServicesButton() {
        otherServicesButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideOtherServicesButton() {
        otherServicesButton.setVisibility(View.GONE);
    }

    @Override
    public String getUsernameEntered() {
        return autoCompleteText.getText().toString();
    }

    @Override
    public void clearSuggestions() {
        adapter.clear();
        adapter.getFilter().filter(autoCompleteText.getText());
    }

}
