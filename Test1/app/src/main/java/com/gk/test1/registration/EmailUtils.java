package com.gk.test1.registration;

/**
 * Util methods for email address text interpreting, splitting.
 *
 * @author Gabor_Keszthelyi
 */
public class EmailUtils {

    private static final String AT_SYMBOL = "@";

    /**
     * Tells whether the given text is an email fraction that contains at least one char domain fraction after a single '@' character.
     *
     * @param fullText the text to check
     * @return true if there is at least one char domain part in the input
     */
    public boolean containsDomainFraction(String fullText) {
        if (notEmpty(fullText)) {
            int indexOfAtSymbol = fullText.indexOf(AT_SYMBOL);
            if (indexOfAtSymbol > -1 && indexOfAtSymbol == fullText.lastIndexOf(AT_SYMBOL) && indexOfAtSymbol < fullText.length() - 1) {
                return true;
            }
        }
        return false;
    }

    private boolean notEmpty(String text) {
        return text != null && !text.isEmpty();
    }

    /**
     * Gets the domain part of the email address.
     *
     * @param emailFraction the email address fraction, must be a valid email address fraction
     * @return the domain part
     */
    public String getDomainFraction(String emailFraction) {
        return emailFraction.split(AT_SYMBOL)[1];
    }

    /**
     * Replaces the domain part of the give email (fraction) with the provided domain.
     *
     * @param email  the email, must be a valid email address (domain can be fraction)
     * @param domain the domain to use
     * @return the email with the replaced domain
     */
    public String replaceDomain(String email, String domain) {
        String localPart = email.split(AT_SYMBOL)[0];
        return localPart + AT_SYMBOL + domain;
    }
}
