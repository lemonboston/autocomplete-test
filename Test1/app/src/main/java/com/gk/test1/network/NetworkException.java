package com.gk.test1.network;

/**
 * Exception for signalling error during networking.
 *
 * @author Gabor_Keszthelyi
 */
public class NetworkException extends RuntimeException {

    public NetworkException(Throwable throwable) {
        super(throwable);
    }
}
