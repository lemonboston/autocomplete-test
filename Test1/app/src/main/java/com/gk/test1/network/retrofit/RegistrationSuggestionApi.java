package com.gk.test1.network.retrofit;

import com.gk.test1.data.server.RegistrationSuggestionResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Interface used by Retrofit representing the Registration Suggestion API.
 *
 * @author Gabor_Keszthelyi
 */
public interface RegistrationSuggestionApi {

    String BASE_URL = "https://api.smoothsync.org/";

    @GET("query/@{query}")
    Call<RegistrationSuggestionResponse> queryForSuggestions(@Path("query") String query);

}
