package com.gk.test1.data.server;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * The parsed registration suggestion response.
 *
 * @author Gabor_Keszthelyi
 */
public class RegistrationSuggestionResponse {

    // "wildcard_matches" isn't seemed to be used any more by API

    public List<String> suggestions;

    @SerializedName("exact_matches")
    public List<ExactMatchesJson> exactMatches;
}
