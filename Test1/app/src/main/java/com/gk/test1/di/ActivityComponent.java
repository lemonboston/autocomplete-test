package com.gk.test1.di;

import com.gk.test1.registration.RegistrationActivity;

import dagger.Component;

/**
 * Dagger Component for the Activities.
 *
 * @author Gabor_Keszthelyi
 */
@ActivityScope
@Component(dependencies = AppComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void satisfy(RegistrationActivity registrationActivity);

}
