package com.gk.test1.di;


import com.gk.test1.base.App;
import com.gk.test1.registration.RegistrationActivity;

/**
 * Handles the Dagger injection calls for all components.
 * So only Injector.satisfy(this) has to be called at injection places, and all related boilerplate is collected here for better overview.
 *
 * @author Gabor_Keszthelyi
 */
public class Injector {

    private static class AppComponentHolder {

        private static AppComponent instance;

        public static void init(App app) {
            instance = DaggerAppComponent
                    .builder()
                    .appModule(new AppModule(app))
                    .build();
        }

        public static AppComponent getInstance() {
            if (instance == null) {
                throw new RuntimeException(AppComponent.class.getSimpleName() + " has not been initialized.");
            }
            return instance;
        }

    }

    public static void init(App app) {
        AppComponentHolder.init(app);
    }

    public static void satisfy(RegistrationActivity registrationActivity) {
        DaggerActivityComponent.builder()
                .appComponent(AppComponentHolder.getInstance())
                .activityModule(new ActivityModule(registrationActivity))
                .build()
                .satisfy(registrationActivity);
    }

}
