package com.gk.test1.registration;

import java.util.List;

/**
 * View component interface for the Registration screen.
 *
 * @author Gabor_Keszthelyi
 */
public interface RegistrationView {

    /**
     * Initializes the UI.
     */
    void init();

    /**
     * Sets the listener who will receive the relevant user action events.
     *
     * @param userActionListener listner
     */
    void setUserActionListener(UserActionListener userActionListener);

    /**
     * Updates the email suggestions for the username input text box.
     *
     * @param emailSuggestions list of suggested email addresses to show
     */
    void updateSuggestions(List<String> emailSuggestions);

    /**
     * Changes the sign in button title.
     *
     * @param text the title to change
     */
    void setSignInButtonText(String text);

    /**
     * Sets if the Sign In button is enabled or not.
     *
     * @param enabled true if enabled
     */
    void setSignInButtonEnabled(boolean enabled);

    /**
     * Shows the 'Other services' button.
     */
    void showOtherServicesButton();

    /**
     * Hides the "other services' button.
     */
    void hideOtherServicesButton();

    /**
     * Retrieves the currently entered text from the username input text box.
     *
     * @return the current text
     */
    String getUsernameEntered();

    /**
     * Clear the suggestions for the autocomplete text box.
     */
    void clearSuggestions();

    /**
     * Interface through which the View can notifiy the Controller about relevant user actions. (As in MVP)
     */
    interface UserActionListener {

        /**
         * Called when a new character is entered in the username input text box.
         *
         * @param newText the whole new text
         * @param wasCharDeleted was it a character deletion or not
         */
        void onUsernameCharTyped(String newText, boolean wasCharDeleted);

        /**
         * Called when the user taps the Sign In button.
         */
        void onSignInButtonClick();

        /**
         * Called when the 'Other Services' button is clicked.
         */
        void onOtherServicesButtonClick();

    }
}
