package com.gk.test1.registration;

import android.os.Bundle;

import com.gk.test1.base.BaseActivity;
import com.gk.test1.di.Injector;

import javax.inject.Inject;


/**
 * Activity for the Registration screen.
 *
 * @author Gabor_Keszthelyi
 */
public class RegistrationActivity extends BaseActivity {

    @Inject
    RegistrationController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Injector.satisfy(this);

        controller.onCreate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        controller.onDestroy();
    }
}
