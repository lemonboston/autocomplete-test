package com.gk.test1.registration;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Unit test for {@link EmailUtils}.
 *
 * @author Gabor_Keszthelyi
 */
public class EmailUtilsTest {

    private EmailUtils emailUtils = new EmailUtils();

    @Test
    public void testContainsDomainFraction() {
        assertFalse(emailUtils.containsDomainFraction(null));
        assertFalse(emailUtils.containsDomainFraction(""));
        assertFalse(emailUtils.containsDomainFraction("a"));
        assertFalse(emailUtils.containsDomainFraction("a123"));
        assertFalse(emailUtils.containsDomainFraction("a123@"));
        assertFalse(emailUtils.containsDomainFraction("a123@@"));
        assertFalse(emailUtils.containsDomainFraction("a123@aa@"));

        assertTrue(emailUtils.containsDomainFraction("a123@b"));
        assertTrue(emailUtils.containsDomainFraction("a123@bbb"));
        assertTrue(emailUtils.containsDomainFraction("a123@b.c"));
    }

    @Test
    public void testGetDomainFraction() {
        assertEquals("domain", emailUtils.getDomainFraction("a@domain"));
        assertEquals("domain.com", emailUtils.getDomainFraction("a@domain.com"));
    }

    @Test
    public void testReplaceDomain() {
        assertEquals("local@second.org", emailUtils.replaceDomain("local@first", "second.org"));
        assertEquals("local@second.org", emailUtils.replaceDomain("local@first.com", "second.org"));
    }
}